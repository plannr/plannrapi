<?php 

include_once 'connexion.php';

$query_serie = $mysqli->query("SELECT * from series JOIN chaines on series.id_chaine = chaines.id_chaine WHERE nom_serie LIKE '%$serie%'");

$nb_serie = $query_serie->num_rows;

$res = array();

if ($nb_serie == 0) {
	$actual_time = $_SERVER['REQUEST_TIME'];
	$date_db = date("Y-m-d",$actual_time);
	$absolute_number = 0;
	$totalep = 0;


	if ($serie_search = @simplexml_load_file('http://thetvdb.com/api/GetSeries.php?seriesname='.$serie)){
		foreach ($serie_search->Series as $serie) {
		
			$key = $serie->seriesid;
			
			if ($series = @simplexml_load_file('http://thetvdb.com/api/17CFE9A03C551B87/series/'.$key.'/all/fr.xml')){

				$insert = 1;

				foreach ($series->Episode as $episode){
					if ($episode->Combined_season != 0) {
						$id_episode = $episode->id;
						$id_serie = $key;
						$saison_episode = $episode->Combined_season;
						$nbsaison_episode = $episode->EpisodeNumber;
						if($absolutenb = $episode->absolute_number){
							if (!is_numeric($absolutenb)) {
								$absolute_number++;
								$absolutenb = $absolute_number;
							}
						}else{
							if ($saison_episode = 1) {
								$absolutenb = $nbsaison_episode;
								$absolute_number = $absolutenb;
							}else{
								$absolute_number++;
								$absolutenb = $absolute_number;	
							}
						}
						$title	= addslashes($episode->EpisodeName);
						$diff	= $episode->FirstAired;
						$resume = addslashes($episode->Overview);

						$img_epi = $key."-".$id_episode.".jpg";

						$img = $episode->filename;
						if ($img != "") {
							$url = 'http://thetvdb.com/banners/'.$img;
							$img = '../httpdocs/show/'.$img_epi;
							file_put_contents($img, file_get_contents($url));
						}else{
							$img_epi = "empty-episode.jpg";
						}

						$diff_epi = strtotime($diff);
						if ($actual_time > $diff_epi) {
							$totalep++;
							$total_saison = $saison_episode;
						}

						$query = $mysqli->query("INSERT INTO episodes VALUES ('".$id_episode."', ".$id_serie.", ".$saison_episode.", ".$nbsaison_episode.", ".$absolute_number.", '".$title."', '".$diff_epi."', '".$resume."', '".$img_epi."', 0, 0, 0)");
					}

				}

				

			    foreach ($series->Series as $serie){
			        $id 	= $serie->id;
			        $title  = addslashes($serie->SeriesName);
			        $genre	= $serie->Genre;
			        $statut = $serie->Status;
			        if ($statut == "Continuing") {
			        	$statut = 2;
			        }

			        if ($statut == "Ended") {
			        	$statut = 1;
			        }

			        $heure = $serie->Airs_Time;
			        if ($heure != "") {
			        	$start_heure = substr($heure, 0, 2);
			        	$end_heure = substr($heure, -2);
			        	
			        	if ($start_heure == "12" && $end_heure == "PM") {
			        		$minutes = substr($heure, 3, 2);
			        		$heure = "00:";
			        		$heure .= $minutes.":00";
			        	}else{
			        		$heure = date('H:i', strtotime($heure));
			        		$heure = $heure.":00";
			        	}
			        }else{
			        	$heure = "00:00:00";
			        }

			        $chaine = $serie->Network;

			        $query_chaine = $mysqli->query("SELECT id_chaine FROM chaines WHERE nom_chaine = '".$chaine."'");
			        $nb_chaine_found = $query_chaine->num_rows;

			        if ($nb_chaine_found == 0) {
			        	$query = $mysqli->query("INSERT INTO chaines VALUES ('','".$chaine."')");
			        	$query_chaine = $mysqli->query("SELECT id_chaine FROM chaines WHERE nom_chaine = '".$chaine."'");
			        	$chaine = $query_chaine->fetch_row();
			        	$chaine = $chaine[0];
			        }else{
			        	$chaine = $query_chaine->fetch_row();
			        	$chaine = $chaine[0];		        	
			        }


			       	// if ($chaine == "AMC") {
			       	// 	$chaine = 1;
			       	// }elseif ($chaine == "FX") {
			       	// 	$chaine = 2;
			       	// }elseif ($chaine == "A&E") {
			       	// 	$chaine = 3;
			       	// }elseif ($chaine == "BBC America") {
			       	// 	$chaine = 4;
			       	// }elseif ($chaine == "HBO") {
			       	// 	$chaine = 5;
			       	// }elseif ($chaine == "Showtime") {
			       	// 	$chaine = 6;
			       	// }elseif ($chaine == "Starz!") {
			       	// 	$chaine = 7;
			       	// }elseif ($chaine == "The WB") {
			       	// 	$chaine = 8;
			       	// }elseif ($chaine == "CBS") {
			       	// 	$chaine = 9;
			       	// }elseif ($chaine == "ABC (US)") {
			       	// 	$chaine = 10;
			       	// }elseif ($chaine == "Syndicated") {
			       	// 	$chaine = 11;
			       	// }elseif ($chaine == "TV Tokyo") {
			       	// 	$chaine = 12;
			       	// }elseif ($chaine == "NBC") {
			       	// 	$chaine = 13;
			       	// }elseif ($chaine == "SBS") {
			       	// 	$chaine = 14;
			       	// }else{
			       	// 	echo $chaine." n'existe pas dans la BDD pour le moment.";
			       	// }

			       	$duree = $serie->Runtime;
			       	$diff = $serie->FirstAired;
			       	$resume = addslashes($serie->Overview);
			       	$rating = $serie->Rating;

			       	$genre = substr($genre, 1);
			       	$genre = substr($genre, 0, -1);
			       	$genre = explode('|', $genre);
			       	// echo "<h2>GENRE</h2>";
			       	foreach ($genre as $val) {
			       		if ($val == "Talk Show" || $val == "Documentary" || $val == "Reality") {
			       			$insert = 0;
			       		}else{
			       			$query = $mysqli->query("SELECT id_genre FROM genres WHERE nom_genre = '".$val."' OR nom_genre_en = '".$val."'");
			       			$nb = $query->num_rows;
			       			if ($nb == 0) {
			       				echo $val." n'existe pas dans la BDD pour le moment. <br>"; 
			       			}else{
			       				$genre = $query->fetch_row();
				       		
					       		$query = $mysqli->query("INSERT INTO gen_ser VALUES ('','".$id."','".$genre[0]."')");
			       			}
			       		}
			       		
			       		

			       	}

			       	if ($insert == 1) {
				       	$img_ser = $serie->fanart;
				       	if ($img_ser != "") {
				       		$url = 'http://thetvdb.com/banners/'.$img_ser;
				       		$img = '../httpdocs/show/'.$id.'.jpg';
				       		file_put_contents($img, file_get_contents($url));
				       	}else{
				       		$img_ser = "empty-serie.jpg";
				       	}
			
				       	$img = $key.".jpg";

			       		$query = $mysqli->query("INSERT INTO series VALUES (".$id.",'".$title."',".$statut.",'".$img."','".$heure."',".$chaine.",".$duree.",'".$total_saison."','".$totalep."','0','0','0','".$diff."','".$resume."', '".$rating."', '".$date_db."', '".$date_db."')");

				       	$poster = $serie->poster;

				       	if ($poster != "") {
				       		$url = 'http://thetvdb.com/banners/'.$poster;
					       	$img = '../httpdocs/show/poster-'.$id.'.jpg';
					       	file_put_contents($img, file_get_contents($url));
				       	}

		       			$actors = @simplexml_load_file('http://thetvdb.com/api/17CFE9A03C551B87/series/'.$key.'/actors.xml');
		       	   		foreach ($actors->Actor as $actor) {
		       	   			$id_role = $actor->id;
		       	   			$role = $actor->Role;
		       	   			$acteur = $actor->Name;
		       	   			$importance = $actor->SortOrder;
		       	   			$acteur_img = $key."-".$id_role.".jpg";
		       	   			$img = $actor->Image;
		       	   			$url = 'http://thetvdb.com/banners/'.$img;

		       	   			$img = '../httpdocs/show/role/'.$acteur_img;
		       	   			file_put_contents($img, file_get_contents($url));

		       	   			$query = $mysqli->query("INSERT INTO roles VALUES (".$id_role.",'".$acteur."','".$role."','".$acteur_img."',".$importance.",".$key.")");
		       	   		}
			       	}else{
			       		$delete = $mysqli->query("DELETE FROM episodes WHERE id_serie = $id_serie");
			       	}
			    }
			}
		}
		
	}
	
}

$query_serie_n = $mysqli->query("SELECT * from series JOIN chaines on series.id_chaine = chaines.id_chaine WHERE nom_serie LIKE '%$serie%'");


while ($row_serie = $query_serie_n->fetch_array()){
	if ($row_serie[statut_serie] == 1) {
		$statut = "Terminée";
	}else if ($row_serie[statut_serie] == 2) {
		$statut = "En cours";
	}else{
		$statut = "";
	}

	array_push($res, ['name' => $row_serie[nom_serie], 'resume' => $row_serie[resume_serie], 'image' => 'http://www.plannr.fr/show/poster-'.$row_serie[img_serie], 'channel' => $row_serie[nom_chaine], 'runtime' => $row_serie[duree_serie], 'season' => $row_serie[saison_serie], 'episode' => $row_serie[episode_serie], 'rating' => $row_serie[rating_serie], 'status' => $statut]);
}


$res_json = json_encode($res);

echo $res_json;


 ?>