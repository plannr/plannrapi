<?php 

if (isset($_GET["id"]) && !empty($_GET["id"])) {


	$key = $_GET["id"];

	$update = $_GET["update"];

	include_once 'connexion.php';

	$actual_time = $_SERVER['REQUEST_TIME'];
	$date_db = date("Y-m-d",$actual_time);


	if ($update == "image") {
		$absolute_number = 0;
		$totalep = 0;

		echo 'http://thetvdb.com/api/17CFE9A03C551B87/series/'.$key.'/all/fr.xml';

		if ($series = @simplexml_load_file('http://thetvdb.com/api/17CFE9A03C551B87/series/'.$key.'/all/fr.xml')){


			foreach ($series->Episode as $episode){
			 	if ($episode->Combined_season != 0) {
			 		$id_episode = $episode->id;

					$img_epi = $key."-".$id_episode.".jpg";

					$img = $episode->filename;
					if ($img != "") {
						$url = 'http://thetvdb.com/banners/'.$img;
						$img = '../httpdocs/show/'.$img_epi;
						file_put_contents($img, file_get_contents($url));
					}else{
						$img_epi = "empty-episode.jpg";
					}

			
			 		$query = $mysqli->query("UPDATE episodes SET img_episode = '".$img_epi."' WHERE id_episode = '".$id_episode."' AND id_serie = '".$key."'");

			 		$query = $mysqli->query("UPDATE series SET date_update_serie = '".$date_db."' WHERE id_serie = '".$key."'");
			 	}

			}

				

		    foreach ($series->Series as $serie){	  
		       	$img_ser = $serie->fanart;
		       	if ($img_ser != "") {
		       		$url = 'http://thetvdb.com/banners/'.$img_ser;
		       		$img = '../httpdocs/show/'.$key.'.jpg';
		       		file_put_contents($img, file_get_contents($url));
		       	}else{
		       		$img_ser = "empty-serie.jpg";
		       	}
	
		       	$img = $key.".jpg";

		       	$poster = $serie->poster;

		       	if ($poster != "") {
		       		$url = 'http://thetvdb.com/banners/'.$poster;
			       	$img = '../httpdocs/show/poster-'.$key.'.jpg';
			       	file_put_contents($img, file_get_contents($url));
		       	}
	       	
		    }
		}
		
	}

	if ($update == "acteur") {
		echo 'http://thetvdb.com/api/17CFE9A03C551B87/series/'.$key.'/all/fr.xml';

		$actors = @simplexml_load_file('http://thetvdb.com/api/17CFE9A03C551B87/series/'.$key.'/actors.xml');
   		foreach ($actors->Actor as $actor) {
   			$id_role = $actor->id;
   			$role = $actor->Role;
   			$acteur = $actor->Name;
   			$importance = $actor->SortOrder;
   			$acteur_img = $key."-".$id_role.".jpg";
   			$img = $actor->Image;
   			$url = 'http://thetvdb.com/banners/'.$img;

   			$img = '../httpdocs/show/role/'.$acteur_img;
   			file_put_contents($img, file_get_contents($url));

   			$query = $mysqli->query("INSERT INTO roles VALUES (".$id_role.",'".$acteur."','".$role."','".$acteur_img."',".$importance.",".$key.")");

   			$query = $mysqli->query("UPDATE series SET date_update_serie = '".$date_db."' WHERE id_serie = '".$key."'");
   		}
	}

	if ($update == "date_episode") {
		if ($series = @simplexml_load_file('http://thetvdb.com/api/17CFE9A03C551B87/series/'.$key.'/all/fr.xml')){


			foreach ($series->Episode as $episode){
			 	if ($episode->Combined_season != 0) {
			 		$id_episode = $episode->id;

					$date = $episode->FirstAired;

					$real_date =  strtotime($date);

					$query = $mysqli->query("UPDATE episodes SET diff_episode = '".$real_date."' WHERE id_episode = '".$id_episode."' AND id_serie = '".$key."'");

					$query = $mysqli->query("UPDATE series SET date_update_serie = '".$date_db."' WHERE id_serie = '".$key."'");
			 	}

			}
		}
	}

	if ($update == "episode") {
		$absolute_number = 0;
		$totalep = 0;

		echo 'http://thetvdb.com/api/17CFE9A03C551B87/series/'.$key.'/all/fr.xml<br><br><br>';

		if ($series = @simplexml_load_file('http://thetvdb.com/api/17CFE9A03C551B87/series/'.$key.'/all/fr.xml')){

			$insert = 1;

			foreach ($series->Episode as $episode){
				if ($episode->Combined_season != 0) {
					$id_episode = $episode->id;

					$select_episode = $mysqli->query("SELECT * FROM episodes WHERE id_episode = '".$id_episode."'");
					$nb_episode = $select_episode->num_rows;



					$id_serie = $key;
					$saison_episode = $episode->Combined_season;
					$nbsaison_episode = $episode->EpisodeNumber;
					if($absolutenb = $episode->absolute_number){
						if (!is_numeric($absolutenb)) {
							$absolute_number++;
							$absolutenb = $absolute_number;
						}
					}else{
						if ($saison_episode = 1) {
							$absolutenb = $nbsaison_episode;
							$absolute_number = $absolutenb;
						}else{
							$absolute_number++;
							$absolutenb = $absolute_number;	
						}
					}
					$title	= addslashes($episode->EpisodeName);
					$diff	= $episode->FirstAired;
					$resume = addslashes($episode->Overview);

					$img_epi = $key."-".$id_episode.".jpg";

					$img = $episode->filename;
					if ($img != "") {
						if ($nb_episode != 1) {
							$url = 'http://thetvdb.com/banners/'.$img;
							$img = '../httpdocs/show/'.$img_epi;
							file_put_contents($img, file_get_contents($url));
						}
					}else{
						$img_epi = "empty-episode.jpg";
					}

					$diff_epi = strtotime($diff);
					if ($actual_time > $diff_epi) {
						$totalep++;
						$total_saison = $saison_episode;
					}

					if ($nb_episode != 1) {
						$delete = $mysqli->query("DELETE FROM episodes WHERE id_serie = $id_serie AND id_episode = '".$id_episode."'");
					}

					$query = $mysqli->query("INSERT INTO episodes VALUES ('".$id_episode."', ".$id_serie.", ".$saison_episode.", ".$nbsaison_episode.", ".$absolute_number.", '".$title."', '".$diff_epi."', '".$resume."', '".$img_epi."', 0, 0, 0)");

					echo 'INSERT INTO episodes VALUES ("'.$id_episode.'", '.$id_serie.', '.$saison_episode.', '.$nbsaison_episode.', '.$absolute_number.', "'.$title.'", "'.$diff_epi.'", "'.$resume.'", "'.$img_epi.'", 0, 0, 0)<br><br>';
				}

			}


			$query = $mysqli->query("UPDATE series SET date_update_serie = '".$date_db."', saison_serie = '".$total_saison."', episode_serie = '".$totalep."' WHERE id_serie = '".$key."'");
		}
	}

	if ($update == "banner") {

		$query_serie = $mysqli->query("SELECT id_serie FROM series");
		while ($row_serie = $query_serie->fetch_array()) {
			$key = $row_serie[id_serie];
		
			if ($series = @simplexml_load_file('http://thetvdb.com/api/17CFE9A03C551B87/series/'.$key.'/all/fr.xml')){
					

			    foreach ($series->Series as $serie){	  
			    	
			       	$img_ser = $serie->banner;
			       	if ($img_ser != "") {
			       		$url = 'http://thetvdb.com/banners/'.$img_ser;
			       		$img = '../httpdocs/show/banner-'.$key.'.jpg';
			       		file_put_contents($img, file_get_contents($url));
			       	}

			       	// $poster = $serie->poster;

			       	// if ($poster != "") {
			       	// 	$url = 'http://thetvdb.com/banners/'.$poster;
				       // 	$img = '../httpdocs/show/poster-'.$key.'.jpg';
				       // 	file_put_contents($img, file_get_contents($url));
			       	// }


		       	
			    }
			}


			// $query = $mysqli->query("UPDATE series SET date_update_serie = '".$date_db."' WHERE id_serie = '".$key."'");
		}

	}


}




